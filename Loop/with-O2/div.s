	.arch armv8-a
	.file	"div.c"
	.text
	.section	.rodata.str1.8,"aMS",@progbits,1
	.align	3
.LC0:
	.string	"Sum of two array elements are:"
	.align	3
.LC1:
	.string	"%d\n"
	.section	.text.startup,"ax",@progbits
	.align	2
	.p2align 4,,11
	.global	main
	.type	main, %function
main:
.LFB11:
	.cfi_startproc
	sub	sp, sp, #2160
	.cfi_def_cfa_offset 2160
	mov	w0, 0
	sub	sp, sp, #81920
	.cfi_def_cfa_offset 84080
	add	x1, sp, 4064
	stp	x29, x30, [sp]
	.cfi_offset 29, -84080
	.cfi_offset 30, -84072
	mov	x29, sp
	stp	x19, x20, [sp, 16]
	str	x21, [sp, 32]
	.cfi_offset 19, -84064
	.cfi_offset 20, -84056
	.cfi_offset 21, -84048
	.p2align 3,,7
.L2:
	str	w0, [x1], 4
	add	w0, w0, 1
	cmp	w0, 1001
	bne	.L2
	mov	x3, 44072
	add	x1, sp, x3
	mov	w0, 0
	.p2align 3,,7
.L3:
	str	w0, [x1], 4
	add	w0, w0, 2
	cmp	w0, 2002
	bne	.L3
	mov	x1, 1
	.p2align 3,,7
.L4:
	lsl	x0, x1, 2
	add	x3, sp, 4064
	mov	x2, 44072
	add	x2, sp, x2
	add	x2, x2, x0
	add	x3, x3, x0
	add	x4, sp, 56
	add	x1, x1, 1
	add	x0, x4, x0
	ldr	w2, [x2, -4]
	ldr	w3, [x3, -4]
	sdiv	w2, w2, w3
	str	w2, [x0, -4]
	cmp	x1, 1001
	bne	.L4
	adrp	x20, .LC1
	add	x20, x20, :lo12:.LC1
	mov	x19, x4
	add	x21, x4, 4000
	adrp	x0, .LC0
	add	x0, x0, :lo12:.LC0
	bl	puts
	.p2align 3,,7
.L5:
	ldr	w1, [x19], 4
	mov	x0, x20
	bl	printf
	cmp	x21, x19
	bne	.L5
	ldp	x29, x30, [sp]
	mov	w0, 0
	ldp	x19, x20, [sp, 16]
	ldr	x21, [sp, 32]
	.cfi_restore 29
	.cfi_restore 30
	.cfi_restore 21
	.cfi_restore 19
	.cfi_restore 20
	add	sp, sp, 2160
	.cfi_def_cfa_offset 81920
	add	sp, sp, 81920
	.cfi_def_cfa_offset 0
	ret
	.cfi_endproc
.LFE11:
	.size	main, .-main
	.ident	"GCC: (Spack GCC) 12.2.0"
	.section	.note.GNU-stack,"",@progbits
