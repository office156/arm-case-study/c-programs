	.arch armv8-a
	.file	"sub.c"
	.text
	.section	.rodata
	.align	3
.LC0:
	.string	"Sum of two array elements are:"
	.align	3
.LC1:
	.string	"%d\n"
	.text
	.align	2
	.global	main
	.type	main, %function
main:
.LFB0:
	.cfi_startproc
	sub	sp, sp, #2128
	.cfi_def_cfa_offset 2128
	sub	sp, sp, #81920
	.cfi_def_cfa_offset 84048
	stp	x29, x30, [sp]
	.cfi_offset 29, -84048
	.cfi_offset 30, -84040
	mov	x29, sp
	add	x0, sp, 81920
	str	wzr, [x0, 2120]
	b	.L2
.L3:
	add	x0, sp, 81920
	ldrsw	x0, [x0, 2120]
	lsl	x0, x0, 2
	add	x1, sp, 40960
	add	x1, x1, 3072
	add	x2, sp, 81920
	ldr	w2, [x2, 2120]
	str	w2, [x1, x0]
	add	x0, sp, 81920
	ldr	w0, [x0, 2120]
	add	w0, w0, 1
	add	x1, sp, 81920
	str	w0, [x1, 2120]
.L2:
	add	x0, sp, 81920
	ldr	w0, [x0, 2120]
	cmp	w0, 1000
	ble	.L3
	add	x0, sp, 81920
	str	wzr, [x0, 2116]
	b	.L4
.L5:
	add	x0, sp, 81920
	ldr	w0, [x0, 2116]
	lsl	w2, w0, 1
	add	x0, sp, 81920
	ldrsw	x0, [x0, 2116]
	lsl	x0, x0, 2
	add	x1, sp, 4024
	str	w2, [x1, x0]
	add	x0, sp, 81920
	ldr	w0, [x0, 2116]
	add	w0, w0, 1
	add	x1, sp, 81920
	str	w0, [x1, 2116]
.L4:
	add	x0, sp, 81920
	ldr	w0, [x0, 2116]
	cmp	w0, 1000
	ble	.L5
	add	x0, sp, 81920
	str	wzr, [x0, 2124]
	b	.L6
.L7:
	add	x0, sp, 81920
	ldrsw	x0, [x0, 2124]
	lsl	x0, x0, 2
	add	x1, sp, 4024
	ldr	w1, [x1, x0]
	add	x0, sp, 81920
	ldrsw	x0, [x0, 2124]
	lsl	x0, x0, 2
	add	x2, sp, 40960
	add	x2, x2, 3072
	ldr	w0, [x2, x0]
	sub	w2, w1, w0
	add	x0, sp, 81920
	ldrsw	x0, [x0, 2124]
	lsl	x0, x0, 2
	add	x1, sp, 16
	str	w2, [x1, x0]
	add	x0, sp, 81920
	ldr	w0, [x0, 2124]
	add	w0, w0, 1
	add	x1, sp, 81920
	str	w0, [x1, 2124]
.L6:
	add	x0, sp, 81920
	ldr	w0, [x0, 2124]
	cmp	w0, 999
	ble	.L7
	adrp	x0, .LC0
	add	x0, x0, :lo12:.LC0
	bl	puts
	add	x0, sp, 81920
	str	wzr, [x0, 2124]
	b	.L8
.L9:
	add	x0, sp, 81920
	ldrsw	x0, [x0, 2124]
	lsl	x0, x0, 2
	add	x1, sp, 16
	ldr	w0, [x1, x0]
	mov	w1, w0
	adrp	x0, .LC1
	add	x0, x0, :lo12:.LC1
	bl	printf
	add	x0, sp, 81920
	ldr	w0, [x0, 2124]
	add	w0, w0, 1
	add	x1, sp, 81920
	str	w0, [x1, 2124]
.L8:
	add	x0, sp, 81920
	ldr	w0, [x0, 2124]
	cmp	w0, 999
	ble	.L9
	mov	w0, 0
	ldp	x29, x30, [sp]
	.cfi_restore 29
	.cfi_restore 30
	add	sp, sp, 2128
	.cfi_def_cfa_offset 81920
	add	sp, sp, 81920
	.cfi_def_cfa_offset 0
	ret
	.cfi_endproc
.LFE0:
	.size	main, .-main
	.ident	"GCC: (Spack GCC) 12.2.0"
	.section	.note.GNU-stack,"",@progbits
