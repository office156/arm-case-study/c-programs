	.arch armv8-a
	.file	"mul.c"
	.text
	.section	.rodata.str1.8,"aMS",@progbits,1
	.align	3
.LC0:
	.string	"Sum of two array elements are:"
	.align	3
.LC1:
	.string	"%d\n"
	.section	.text.startup,"ax",@progbits
	.align	2
	.p2align 4,,11
	.global	main
	.type	main, %function
main:
.LFB11:
	.cfi_startproc
	sub	sp, sp, #2176
	.cfi_def_cfa_offset 2176
	adrp	x0, .LC2
	movi	v2.4s, 0x4
	sub	sp, sp, #81920
	.cfi_def_cfa_offset 84096
	add	x4, sp, 4064
	add	x1, x4, 4000
	stp	x29, x30, [sp]
	.cfi_offset 29, -84096
	.cfi_offset 30, -84088
	mov	x29, sp
	ldr	q0, [x0, #:lo12:.LC2]
	mov	x0, x4
	stp	x19, x20, [sp, 16]
	str	x21, [sp, 32]
	.cfi_offset 19, -84080
	.cfi_offset 20, -84072
	.cfi_offset 21, -84064
	.p2align 3,,7
.L2:
	mov	v1.16b, v0.16b
	add	v0.4s, v0.4s, v2.4s
	str	q1, [x0], 16
	cmp	x0, x1
	bne	.L2
	adrp	x2, .LC2
	mov	x0, 44080
	movi	v1.4s, 0x4
	add	x0, sp, x0
	ldr	q0, [x2, #:lo12:.LC2]
	add	x3, x0, 4000
	mov	x2, x0
	mov	w1, 1000
	str	w1, [sp, 8064]
.L3:
	mov	v2.16b, v0.16b
	add	v0.4s, v0.4s, v1.4s
	shl	v2.4s, v2.4s, 1
	str	q2, [x2], 16
	cmp	x3, x2
	bne	.L3
	add	x5, sp, 32768
	add	x19, sp, 48
	mov	x2, x19
	mov	w1, 2000
	str	w1, [x5, 15312]
.L4:
	ldr	q0, [x4], 16
	ldr	q1, [x0], 16
	mul	v0.4s, v0.4s, v1.4s
	str	q0, [x2], 16
	cmp	x3, x0
	bne	.L4
	adrp	x20, .LC1
	add	x21, x19, 4000
	add	x20, x20, :lo12:.LC1
	adrp	x0, .LC0
	add	x0, x0, :lo12:.LC0
	bl	puts
	.p2align 3,,7
.L5:
	ldr	w1, [x19], 4
	mov	x0, x20
	bl	printf
	cmp	x21, x19
	bne	.L5
	ldp	x29, x30, [sp]
	mov	w0, 0
	ldp	x19, x20, [sp, 16]
	ldr	x21, [sp, 32]
	.cfi_restore 29
	.cfi_restore 30
	.cfi_restore 21
	.cfi_restore 19
	.cfi_restore 20
	add	sp, sp, 2176
	.cfi_def_cfa_offset 81920
	add	sp, sp, 81920
	.cfi_def_cfa_offset 0
	ret
	.cfi_endproc
.LFE11:
	.size	main, .-main
	.section	.rodata.cst16,"aM",@progbits,16
	.align	4
.LC2:
	.word	0
	.word	1
	.word	2
	.word	3
	.ident	"GCC: (Spack GCC) 12.2.0"
	.section	.note.GNU-stack,"",@progbits
