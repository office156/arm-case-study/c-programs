	.arch armv8-a
	.file	"div.c"
	.text
	.align	2
	.global	main
	.type	main, %function
main:
.LFB0:
	.cfi_startproc
	mov	x12, 13360
	movk	x12, 0x503, lsl 16
	sub	sp, sp, x12
	.cfi_def_cfa_offset 84096048
	mov	x0, 84082688
	add	x0, sp, x0
	str	wzr, [x0, 13348]
	b	.L2
.L5:
	mov	x0, 84082688
	add	x0, sp, x0
	str	wzr, [x0, 13344]
	b	.L3
.L4:
	mov	x0, 84082688
	add	x0, sp, x0
	ldrsw	x0, [x0, 13344]
	mov	x1, 84082688
	add	x1, sp, x1
	ldrsw	x2, [x1, 13348]
	mov	x1, 1001
	mul	x1, x2, x1
	add	x0, x1, x0
	lsl	x0, x0, 2
	mov	x1, -1536
	movk	x1, 0xfd9c, lsl 16
	mov	x2, 13360
	movk	x2, 0x503, lsl 16
	add	x2, x2, x1
	add	x1, sp, x2
	mov	x2, 84082688
	add	x2, sp, x2
	ldr	w2, [x2, 13344]
	str	w2, [x1, x0]
	mov	x0, 84082688
	add	x0, sp, x0
	ldr	w0, [x0, 13344]
	add	w0, w0, 1
	mov	x1, 84082688
	add	x1, sp, x1
	str	w0, [x1, 13344]
.L3:
	mov	x0, 84082688
	add	x0, sp, x0
	ldr	w0, [x0, 13344]
	cmp	w0, 1000
	ble	.L4
	mov	x0, 84082688
	add	x0, sp, x0
	ldr	w0, [x0, 13348]
	add	w0, w0, 1
	mov	x1, 84082688
	add	x1, sp, x1
	str	w0, [x1, 13348]
.L2:
	mov	x0, 84082688
	add	x0, sp, x0
	ldr	w0, [x0, 13348]
	cmp	w0, 1000
	ble	.L5
	mov	x0, 84082688
	add	x0, sp, x0
	str	wzr, [x0, 13340]
	b	.L6
.L9:
	mov	x0, 84082688
	add	x0, sp, x0
	str	wzr, [x0, 13336]
	b	.L7
.L8:
	mov	x0, 84082688
	add	x0, sp, x0
	ldrsw	x0, [x0, 13336]
	mov	x1, 84082688
	add	x1, sp, x1
	ldrsw	x2, [x1, 13340]
	mov	x1, 1001
	mul	x1, x2, x1
	add	x0, x1, x0
	lsl	x0, x0, 2
	mov	x1, -3048
	movk	x1, 0xfb39, lsl 16
	mov	x2, 13360
	movk	x2, 0x503, lsl 16
	add	x2, x2, x1
	add	x1, sp, x2
	mov	x2, 84082688
	add	x2, sp, x2
	ldr	w2, [x2, 13336]
	str	w2, [x1, x0]
	mov	x0, 84082688
	add	x0, sp, x0
	ldr	w0, [x0, 13336]
	add	w0, w0, 1
	mov	x1, 84082688
	add	x1, sp, x1
	str	w0, [x1, 13336]
.L7:
	mov	x0, 84082688
	add	x0, sp, x0
	ldr	w0, [x0, 13336]
	cmp	w0, 1000
	ble	.L8
	mov	x0, 84082688
	add	x0, sp, x0
	ldr	w0, [x0, 13340]
	add	w0, w0, 1
	mov	x1, 84082688
	add	x1, sp, x1
	str	w0, [x1, 13340]
.L6:
	mov	x0, 84082688
	add	x0, sp, x0
	ldr	w0, [x0, 13340]
	cmp	w0, 1000
	ble	.L9
	mov	x0, 84082688
	add	x0, sp, x0
	str	wzr, [x0, 13356]
	b	.L10
.L13:
	mov	x0, 84082688
	add	x0, sp, x0
	str	wzr, [x0, 13352]
	b	.L11
.L12:
	mov	x0, 84082688
	add	x0, sp, x0
	ldrsw	x0, [x0, 13352]
	mov	x1, 84082688
	add	x1, sp, x1
	ldrsw	x2, [x1, 13356]
	mov	x1, 1001
	mul	x1, x2, x1
	add	x0, x1, x0
	lsl	x0, x0, 2
	mov	x1, -1536
	movk	x1, 0xfd9c, lsl 16
	mov	x2, 13360
	movk	x2, 0x503, lsl 16
	add	x2, x2, x1
	add	x1, sp, x2
	ldr	w0, [x1, x0]
	mov	x1, 84082688
	add	x1, sp, x1
	ldrsw	x1, [x1, 13352]
	mov	x2, 84082688
	add	x2, sp, x2
	ldrsw	x3, [x2, 13356]
	mov	x2, 1001
	mul	x2, x3, x2
	add	x1, x2, x1
	lsl	x1, x1, 2
	mov	x2, -3048
	movk	x2, 0xfb39, lsl 16
	mov	x3, 13360
	movk	x3, 0x503, lsl 16
	add	x3, x3, x2
	add	x2, sp, x3
	ldr	w1, [x2, x1]
	sdiv	w2, w0, w1
	mul	w1, w2, w1
	sub	w2, w0, w1
	mov	x0, 84082688
	add	x0, sp, x0
	ldrsw	x0, [x0, 13352]
	mov	x1, 84082688
	add	x1, sp, x1
	ldrsw	x3, [x1, 13356]
	mov	x1, 1001
	mul	x1, x3, x1
	add	x0, x1, x0
	lsl	x0, x0, 2
	mov	x1, -13360
	movk	x1, 0xfafc, lsl 16
	mov	x3, 13360
	movk	x3, 0x503, lsl 16
	add	x3, x3, x1
	add	x1, sp, x3
	str	w2, [x1, x0]
	mov	x0, 84082688
	add	x0, sp, x0
	ldr	w0, [x0, 13352]
	add	w0, w0, 1
	mov	x1, 84082688
	add	x1, sp, x1
	str	w0, [x1, 13352]
.L11:
	mov	x0, 84082688
	add	x0, sp, x0
	ldr	w0, [x0, 13352]
	cmp	w0, 1000
	ble	.L12
	mov	x0, 84082688
	add	x0, sp, x0
	ldr	w0, [x0, 13356]
	add	w0, w0, 1
	mov	x1, 84082688
	add	x1, sp, x1
	str	w0, [x1, 13356]
.L10:
	mov	x0, 84082688
	add	x0, sp, x0
	ldr	w0, [x0, 13356]
	cmp	w0, 1000
	ble	.L13
	mov	w0, 0
	add	sp, sp, x12
	.cfi_def_cfa_offset 0
	ret
	.cfi_endproc
.LFE0:
	.size	main, .-main
	.ident	"GCC: (Spack GCC) 12.2.0"
	.section	.note.GNU-stack,"",@progbits
